# investment calculator #

All cases provided with application requirements are tested. As application requirements contain only SAFE investment style scenarios, test suite was extended for BALANCED and AGGRESSIVE investment styles.

Running tests with coverage feature provided by IntelliJ IDEA results in 100% coverage for classes and 100% coverage for lines.

Flow of this application starts with `InvestmentFundAmountCalculator` class. Main purpose of this class is handling interface `InvestmentDistributor` in case of other implementations in future. This class uses as well factory implemented with Enum: `InvestmentDistributionStyle`, which return appropriate InvestmentDistribution (`AggressiveInvestmentDistribution`, `BalancedInvestmentDistribution` or `SafeInvestmentDistribution`)

`MultiFundTypesInvestmentDistributor` is implementation of `InvestmentDistributor` used to handle multiple fund types.

`AmountPerFundTypeCalculator` is used to calculate amount per fund type with refund calculation.

`AmountPerFundCalculator` calculates appropriate amount division between same type of fund with handling of excess amount.

Other classes are immutable data model classes implemented with usage of `lombok` for simplification.

Implemented tests are parametrized, so it's very easy to add new testing scenarios.

# Build and tests execution
To build application and execute tests use command:
```mvn clean package```


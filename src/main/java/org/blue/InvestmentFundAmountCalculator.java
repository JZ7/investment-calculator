package org.blue;

import lombok.AllArgsConstructor;
import org.blue.model.CalculatedFunds;
import org.blue.model.Fund;
import org.blue.model.investmentdistribution.InvestmentDistribution;
import org.blue.model.investmentdistribution.InvestmentDistributionStyle;

import java.util.List;

@AllArgsConstructor
public class InvestmentFundAmountCalculator {

    private final int investmentAmount;
    private final InvestmentDistributionStyle investmentDistributionStyle;
    private final List<Fund> funds;
    private final InvestmentDistributor investmentDistributor;

    public CalculatedFunds calculate() {
        InvestmentDistribution investmentDistribution = investmentDistributionStyle.getInvestmentDistribution();

        return investmentDistributor.distribute(investmentDistribution, funds, investmentAmount);
    }
}

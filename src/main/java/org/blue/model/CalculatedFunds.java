package org.blue.model;

import lombok.Data;

import java.util.List;

@Data
public final class CalculatedFunds {

    private final List<CalculatedFund> funds;
    private final int refundAmount;
}

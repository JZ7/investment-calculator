package org.blue.model;

import lombok.Data;

@Data
public final class AmountPerFundType {

    private final int polish;
    private final int foreign;
    private final int money;
    private final int refund;
}

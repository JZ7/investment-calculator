package org.blue.model;

import lombok.Data;

@Data
public final class CalculatedFund {

    private final long number;
    private final Fund fund;
    private final int amount;
    private final float percent;
}

package org.blue.model;

import lombok.Data;

@Data
public final class Fund {

    private final long id;
    private final String name;
    private final FundType type;
}

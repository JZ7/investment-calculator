package org.blue.model.investmentdistribution;

import lombok.Data;

@Data
public class InvestmentDistribution {

    private final int polishPercent;
    private final int foreignPercent;
    private final int moneyPercent;
}

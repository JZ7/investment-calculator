package org.blue.model.investmentdistribution;

public final class SafeInvestmentDistribution extends InvestmentDistribution {

    public SafeInvestmentDistribution() {
        super(20, 75, 5);
    }
}

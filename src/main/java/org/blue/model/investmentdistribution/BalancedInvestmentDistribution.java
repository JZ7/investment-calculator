package org.blue.model.investmentdistribution;

public final class BalancedInvestmentDistribution extends InvestmentDistribution {

    public BalancedInvestmentDistribution() {
        super(30, 60, 10);
    }
}

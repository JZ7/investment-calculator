package org.blue.model.investmentdistribution;

public enum InvestmentDistributionStyle {

    SAFE("safe") {
        @Override
        public InvestmentDistribution getInvestmentDistribution() {
            return new SafeInvestmentDistribution();
        }
    },
    BALANCED("balanced") {
        @Override
        public InvestmentDistribution getInvestmentDistribution() {
            return new BalancedInvestmentDistribution();
        }
    },
    AGGRESSIVE("aggressive") {
        @Override
        public InvestmentDistribution getInvestmentDistribution() {
            return new AggressiveInvestmentDistribution();
        }
    };

    InvestmentDistributionStyle(String type) {
        this.type = type;
    }

    private String type;
    public abstract InvestmentDistribution getInvestmentDistribution();
}

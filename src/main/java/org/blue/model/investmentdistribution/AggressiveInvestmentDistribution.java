package org.blue.model.investmentdistribution;

public final class AggressiveInvestmentDistribution extends InvestmentDistribution {

    public AggressiveInvestmentDistribution() {
        super(40, 20, 40);
    }
}

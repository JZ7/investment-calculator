package org.blue;

import lombok.NoArgsConstructor;
import org.blue.InvestmentDistributor;
import org.blue.fundamountcalculators.AmountPerFundCalculator;
import org.blue.fundamountcalculators.AmountPerFundTypeCalculator;
import org.blue.model.AmountPerFundType;
import org.blue.model.CalculatedFund;
import org.blue.model.CalculatedFunds;
import org.blue.model.Fund;
import org.blue.model.FundType;
import org.blue.model.investmentdistribution.InvestmentDistribution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class MultiFundTypesInvestmentDistributor implements InvestmentDistributor {

    @Override
    public CalculatedFunds distribute(InvestmentDistribution investmentDistribution,
                                      List<Fund> funds,
                                      int investmentAmount) {

        AmountPerFundType amountPerFundType = AmountPerFundTypeCalculator
                .calculate(investmentDistribution, investmentAmount);
        List<CalculatedFund> allCalculatedFunds = new ArrayList<>();

        List<CalculatedFund> polishCalculatedFunds = AmountPerFundCalculator.calculate(
                amountPerFundType.getPolish(),
                investmentDistribution.getPolishPercent(),
                filterFunds(funds, FundType.POLISH));

        List<CalculatedFund> foreignCalculatedFunds = AmountPerFundCalculator.calculate(
                amountPerFundType.getForeign(),
                investmentDistribution.getForeignPercent(),
                filterFunds(funds, FundType.FOREIGN));

        List<CalculatedFund> moneyCalculatedFunds = AmountPerFundCalculator.calculate(
                amountPerFundType.getMoney(),
                investmentDistribution.getMoneyPercent(),
                filterFunds(funds, FundType.MONEY));

        allCalculatedFunds.addAll(polishCalculatedFunds);
        allCalculatedFunds.addAll(foreignCalculatedFunds);
        allCalculatedFunds.addAll(moneyCalculatedFunds);

        return new CalculatedFunds(allCalculatedFunds, amountPerFundType.getRefund());
    }

    private List<Fund> filterFunds(List<Fund> funds, FundType fundType) {
        return funds.stream()
                .filter(fund -> fund.getType().equals(fundType))
                .collect(Collectors.toList());
    }
}

package org.blue;

import org.blue.model.CalculatedFunds;
import org.blue.model.Fund;
import org.blue.model.investmentdistribution.InvestmentDistribution;

import java.util.List;

public interface InvestmentDistributor {

    CalculatedFunds distribute(InvestmentDistribution investmentDistribution, List<Fund> funds, int investmentAmount);
}

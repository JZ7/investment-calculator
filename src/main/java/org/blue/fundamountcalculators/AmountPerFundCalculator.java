package org.blue.fundamountcalculators;

import org.blue.model.CalculatedFund;
import org.blue.model.Fund;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class AmountPerFundCalculator {

    private AmountPerFundCalculator() {
    }

    public static List<CalculatedFund> calculate(int fundTypeAmount, int fundTypePercent, List<Fund> funds) {
        int divisibleAmount = getAmountDivisibleByFunds(fundTypeAmount, funds.size());
        int averageFundAmount = getAverageFundAmount(divisibleAmount, funds.size());
        int firstFundAmount = averageFundAmount + (fundTypeAmount - divisibleAmount);
        List<CalculatedFund> calculatedFunds = new ArrayList<>();

        Iterator<Fund> it = funds.iterator();
        Fund f;
        if (it.hasNext()) {
            f = it.next();
            calculatedFunds.add(getCalculatedFund(fundTypeAmount, fundTypePercent, f, firstFundAmount));
            while (it.hasNext()) {
                f = it.next();
                calculatedFunds.add(getCalculatedFund(fundTypeAmount, fundTypePercent, f, averageFundAmount));
            }
        }
        return  calculatedFunds;
    }

    private static CalculatedFund getCalculatedFund(int fundTypeAmount, int fundTypePercent, Fund fund, int fundAmount) {
        float fundPercent = getFundPercent(fundAmount, fundTypePercent, fundTypeAmount);
        return new CalculatedFund(fund.getId(), fund, fundAmount, fundPercent);
    }

    private static int getAverageFundAmount(int amount, int fundsCount) {
        return amount/fundsCount;
    }

    private static float getFundPercent(int fundAmount, int percent, int fundTypeAmount) {
        return (float)fundAmount * (float)percent / (float)fundTypeAmount;
    }

    private static int getAmountDivisibleByFunds(int amount, int fundsCount) {
        int divisibleAmount = amount;
        while(!isDivisible(divisibleAmount, fundsCount)) {
            divisibleAmount--;
        }
        return divisibleAmount;
    }

    private static boolean isDivisible(int amount, int fundsCount) {
        return amount % fundsCount == 0;
    }
}

package org.blue.fundamountcalculators;

import org.blue.model.AmountPerFundType;
import org.blue.model.investmentdistribution.InvestmentDistribution;

public final class AmountPerFundTypeCalculator {

    private AmountPerFundTypeCalculator() {
    }

    public static AmountPerFundType calculate(InvestmentDistribution investmentDistribution, int amount) {
        int divisibleAmount = getAmountDivisibleByInvestmentDistribution(investmentDistribution, amount);
        int refund = amount - divisibleAmount;
        return new AmountPerFundType(
                getPercentOfAmount(divisibleAmount, investmentDistribution.getPolishPercent()),
                getPercentOfAmount(divisibleAmount, investmentDistribution.getForeignPercent()),
                getPercentOfAmount(divisibleAmount, investmentDistribution.getMoneyPercent()),
                refund);
    }

    private static int getAmountDivisibleByInvestmentDistribution(InvestmentDistribution investmentDistribution,
                                                                  int investmentAmount) {
        int divisibleAmount = investmentAmount;
        while(!isInvestmentAmountDivisible(investmentDistribution, divisibleAmount)) {
            divisibleAmount--;
        }
        return divisibleAmount;
    }

    private static int getPercentOfAmount(int amount, int percent) {
        return amount * percent / 100;
    }

    private static boolean isInvestmentAmountDivisible(InvestmentDistribution investmentDistribution,
                                                       int investmentAmount) {
        return isDivisible(investmentDistribution.getPolishPercent(), investmentAmount) &&
                isDivisible(investmentDistribution.getForeignPercent(), investmentAmount) &&
                isDivisible(investmentDistribution.getMoneyPercent(), investmentAmount);
    }

    private static boolean isDivisible(int percent, int amount) {
        return amount * percent % 100 == 0;
    }
}

package org.blue.fundamountcalculators;

import org.blue.model.CalculatedFund;
import org.blue.model.Fund;
import org.blue.model.FundType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class AmountPerFundCalculatorTest {

    private final int amount;
    private final int percent;
    private final List<Fund> funds;
    private final List<CalculatedFund> calculatedFunds;

    public AmountPerFundCalculatorTest(int amount, int percent, List<Fund> funds, List<CalculatedFund>
            calculatedFunds) {
        this.amount = amount;
        this.percent = percent;
        this.funds = funds;
        this.calculatedFunds = calculatedFunds;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{{
            2000,
            20,
            Arrays.asList(
                    new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                    new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH)),
            Arrays.asList(
                    new CalculatedFund(
                            1,
                            new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                            1000,
                            10f),
                    new CalculatedFund(
                            2,
                            new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                            1000,
                            10f))
            }, {
            2000,
            20,
            Arrays.asList(
                    new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                    new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                    new Fund(3L, FundNames.PL_FUND_3, FundType.POLISH)),
            Arrays.asList(
                    new CalculatedFund(
                            1,
                            new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                            668,
                            6.68f),
                    new CalculatedFund(
                            2,
                            new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                            666,
                            6.66f),
                    new CalculatedFund(
                            3,
                            new Fund(3L, FundNames.PL_FUND_3, FundType.POLISH),
                            666,
                            6.66f))
        },{
                3000,
                30,
                Arrays.asList(
                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH)),
                Arrays.asList(
                        new CalculatedFund(
                                1,
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                1500,
                                15),
                        new CalculatedFund(
                                2,
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                1500,
                                15))
        }
        });
    }

    @Test
    public void test() {
        List<CalculatedFund> actualCalculatedFunds = AmountPerFundCalculator.calculate(amount, percent, funds);

        assertEquals(calculatedFunds.size(), actualCalculatedFunds.size());
        assertThat(actualCalculatedFunds, is(calculatedFunds));
    }
}

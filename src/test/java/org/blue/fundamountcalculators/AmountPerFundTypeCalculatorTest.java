package org.blue.fundamountcalculators;

import org.blue.model.AmountPerFundType;
import org.blue.model.investmentdistribution.AggressiveInvestmentDistribution;
import org.blue.model.investmentdistribution.BalancedInvestmentDistribution;
import org.blue.model.investmentdistribution.InvestmentDistribution;
import org.blue.model.investmentdistribution.SafeInvestmentDistribution;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AmountPerFundTypeCalculatorTest {

    private final InvestmentDistribution investmentDistribution;
    private final int amount;
    private final int refund;
    private final int plAmount;
    private final int foreignAmount;
    private final int moneyAmount;

    public AmountPerFundTypeCalculatorTest(InvestmentDistribution investmentDistribution, int amount, int refund, int plAmount, int
            foreignAmount, int moneyAmount) {
        this.investmentDistribution = investmentDistribution;
        this.amount = amount;
        this.refund = refund;
        this.plAmount = plAmount;
        this.foreignAmount = foreignAmount;
        this.moneyAmount = moneyAmount;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new SafeInvestmentDistribution(), 10000, 0, 2000, 7500, 500},
                {new SafeInvestmentDistribution(), 10001, 1, 2000, 7500, 500},
                {new BalancedInvestmentDistribution(), 10000, 0, 3000, 6000, 1000},
                {new AggressiveInvestmentDistribution(), 10000, 0, 4000, 2000, 4000}
        });
    }

    @Test
    public void test() {
        AmountPerFundType amountPerFundType = AmountPerFundTypeCalculator.calculate(investmentDistribution, amount);

        assertEquals(refund, amountPerFundType.getRefund());
        assertEquals(plAmount, amountPerFundType.getPolish());
        assertEquals(foreignAmount, amountPerFundType.getForeign());
        assertEquals(moneyAmount, amountPerFundType.getMoney());
    }
}

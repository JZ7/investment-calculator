package org.blue.fundamountcalculators;

public class FundNames {

    public static final String PL_FUND_1 = "Fundusz Polski 1";
    public static final String PL_FUND_2 = "Fundusz Polski 2";
    public static final String PL_FUND_3 = "Fundusz Polski 3";
    public static final String FOREIGN_FUND_1 = "Fundusz Zagraniczny 1";
    public static final String FOREIGN_FUND_2 = "Fundusz Zagraniczny 2";
    public static final String FOREIGN_FUND_3 = "Fundusz Zagraniczny 3";
    public static final String MONEY_FUND_1 = "Fundusz Pieniężny 1";

    private FundNames() {
    }
}

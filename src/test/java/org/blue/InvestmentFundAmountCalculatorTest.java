package org.blue;

import org.blue.fundamountcalculators.FundNames;
import org.blue.model.CalculatedFund;
import org.blue.model.CalculatedFunds;
import org.blue.model.Fund;
import org.blue.model.FundType;
import org.blue.model.investmentdistribution.InvestmentDistributionStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class InvestmentFundAmountCalculatorTest {

    private final int amount;
    private final InvestmentDistributionStyle investmentDistributionStyle;
    private final List<Fund> funds;
    private final List<CalculatedFund> calculatedFunds;
    private final int refundAmount;

    public InvestmentFundAmountCalculatorTest(int investmentAmount,
                                              InvestmentDistributionStyle investmentDistributionStyle,
                                              List<Fund> funds,
                                              List<CalculatedFund> calculatedFunds,
                                              int refundAmount) {
        this.amount = investmentAmount;
        this.investmentDistributionStyle = investmentDistributionStyle;
        this.funds = funds;
        this.calculatedFunds = calculatedFunds;
        this.refundAmount = refundAmount;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        10000,
                        InvestmentDistributionStyle.SAFE,
                        Arrays.asList(
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN),
                                new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN),
                                new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN),
                                new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY)),
                        Arrays.asList(
                                new CalculatedFund(1,
                                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH), 1000, 10),
                                new CalculatedFund(2,
                                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH), 1000, 10),
                                new CalculatedFund(3,
                                        new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(4,
                                        new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(5,
                                        new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(6,
                                        new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY), 500, 5)
                        ),
                        0
                },
                {
                        10001,
                        InvestmentDistributionStyle.SAFE,
                        Arrays.asList(
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN),
                                new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN),
                                new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN),
                                new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY)),
                        Arrays.asList(
                                new CalculatedFund(1,
                                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH), 1000, 10),
                                new CalculatedFund(2,
                                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH), 1000, 10),
                                new CalculatedFund(3,
                                        new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(4,
                                        new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(5,
                                        new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN), 2500, 25),
                                new CalculatedFund(6,
                                        new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY), 500, 5)
                        ),
                        1
                },
                {
                        10000,
                        InvestmentDistributionStyle.SAFE,
                        Arrays.asList(
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                new Fund(3L, FundNames.PL_FUND_3, FundType.POLISH),
                                new Fund(4L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN),
                                new Fund(5L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN),
                                new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY)),
                        Arrays.asList(
                                new CalculatedFund(1,
                                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH), 668, 6.68f),
                                new CalculatedFund(2,
                                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH), 666, 6.66f),
                                new CalculatedFund(3,
                                        new Fund(3L, FundNames.PL_FUND_3, FundType.POLISH), 666, 6.66f),
                                new CalculatedFund(4,
                                        new Fund(4L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN), 3750, 37.5f),
                                new CalculatedFund(5,
                                        new Fund(5L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN), 3750, 37.5f),
                                new CalculatedFund(6,
                                        new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY), 500, 5)
                        ),
                        0
                },
                {
                        10000,
                        InvestmentDistributionStyle.BALANCED,
                        Arrays.asList(
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN),
                                new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN),
                                new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN),
                                new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY)),
                        Arrays.asList(
                                new CalculatedFund(1,
                                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH), 1500, 15),
                                new CalculatedFund(2,
                                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH), 1500, 15),
                                new CalculatedFund(3,
                                        new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN), 2000, 20),
                                new CalculatedFund(4,
                                        new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN), 2000, 20),
                                new CalculatedFund(5,
                                        new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN), 2000, 20),
                                new CalculatedFund(6,
                                        new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY), 1000, 10)
                        ),
                        0
                },
                {
                        10000,
                        InvestmentDistributionStyle.AGGRESSIVE,
                        Arrays.asList(
                                new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH),
                                new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH),
                                new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN),
                                new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN),
                                new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN),
                                new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY)),
                        Arrays.asList(
                                new CalculatedFund(1,
                                        new Fund(1L, FundNames.PL_FUND_1, FundType.POLISH), 2000, 20),
                                new CalculatedFund(2,
                                        new Fund(2L, FundNames.PL_FUND_2, FundType.POLISH), 2000, 20),
                                new CalculatedFund(3,
                                        new Fund(3L, FundNames.FOREIGN_FUND_1, FundType.FOREIGN), 668, 6.68f),
                                new CalculatedFund(4,
                                        new Fund(4L, FundNames.FOREIGN_FUND_2, FundType.FOREIGN), 666, 6.66f),
                                new CalculatedFund(5,
                                        new Fund(5L, FundNames.FOREIGN_FUND_3, FundType.FOREIGN), 666, 6.66f),
                                new CalculatedFund(6,
                                        new Fund(6L, FundNames.MONEY_FUND_1, FundType.MONEY), 4000, 40)
                        ),
                        0
                }
        });
    }

    @Test
    public void test() {
        InvestmentDistributor investmentDistributor = new MultiFundTypesInvestmentDistributor();
        InvestmentFundAmountCalculator investmentFundAmountCalculator =
                new InvestmentFundAmountCalculator(amount, investmentDistributionStyle, funds, investmentDistributor);
        CalculatedFunds actualCalculatedFunds = investmentFundAmountCalculator.calculate();

        assertThat(actualCalculatedFunds.getFunds(), is(calculatedFunds));
        assertEquals(refundAmount, actualCalculatedFunds.getRefundAmount());
    }
}
